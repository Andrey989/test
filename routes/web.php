<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'MainController@index')->name('main');



Route::group(['middleware' => ['auth', 'admin']], function() {

    Route::get('/home', 'HomeController@index')->name('home');

});


//--------------------------------------------------------------------


Route::get('/company', 'AllCompanyController@index')->name('index');
Route::get('/company/create', 'AllCompanyController@vieweditor')->name('vieweditor');
Route::post('/company/create', 'AllCompanyController@create')->name('create');

Route::get('/company/more/{id}', 'AllCompanyController@more')->name('more');

Route::get('/wrkusers/edit/{id}', 'WorkUserController@edit')->name('edit');


Route::get('/workuser/create/{id}', 'WorkUserController@index')->name('index');

Route::post('/workuser/create/{id}', 'WorkUserController@create')->name('create');

Route::get('/workuser/destroy/{id}', 'WorkUserController@destroy')->name('destroy');
Route::get('/company/destroy/{id}', 'AllCompanyController@destroy')->name('destroy');

Route::post('/wrkusers/edit/{id}', 'WorkUserController@update')->name('update');

Route::get('/company/edit/{id}', 'AllCompanyController@edit')->name('edit');
Route::post('/company/edit/{id}', 'AllCompanyController@update')->name('update');



//--------------------------------------------------------------------
