<?php

namespace App\Http\Controllers;
use App\Wrkuser;
use Illuminate\Http\Request;

class WorkUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        return view('workuser',compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $company = new Wrkuser;
        $company->name = $request['name'];
        $company->position = $request['position'];
        $company->adress = $request['adress'];
        $company->status = $request['status'];
        $company->phone = $request['phone'];
        $company->userid = $id;
        $company->save();

        return redirect('/workuser/create/'.$id.'');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $wrkuser = Wrkuser::where('id', $id)->first();

        

        return view('wrkuser', compact('wrkuser'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $wrkuser = Wrkuser::where('id', $id)->first();
        $wrkuser->name = $request['name'];
        $wrkuser->phone = $request['phone'];
        $wrkuser->adress = $request['adress'];
        $wrkuser->position = $request['position'];
        $wrkuser->status = $request['status'];
        $wrkuser->save();

        return redirect('/company/more/'.$wrkuser->userid.'');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wrkuser = Wrkuser::where('id', $id)->first();
        $wrkuser->delete();

        return redirect('/wrkusers/more/'.$wrkuser->userid.'');
    }
}
