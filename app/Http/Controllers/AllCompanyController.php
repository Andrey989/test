<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Wrkuser;

class AllCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $companys = Company::All();
        return view('company', compact('companys'));
    }



    public function vieweditor()
    {
        return view('companyedit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $company = new Company;
        $company->title = $request['title'];
        $company->content = $request['content'];
        $company->adress = $request['adress'];
        $company->working = $request['working'];
        $company->phone = $request['phone'];

        $company->save();

        return redirect('/company');




        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
        $company = Company::where('id', $id)->first();
        return view('companyupdate', compact('company'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $company = Company::where('id', $id)->first();
        $company->title = $request['title'];
        $company->content = $request['content'];
        $company->adress = $request['adress'];
        $company->working = $request['working'];
        $company->phone = $request['phone'];
        $company->save();

        return redirect('/company');

    }

    public function more(Request $request, $id)
    {

        $company = Company::where('id', $id)->first();
        $wrkusers = Wrkuser::where('userid', $id)->get();

        return view('more', compact('company','wrkusers'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::where('id', $id)->first();
        $company->delete();

        return redirect('/company');
    }
}
