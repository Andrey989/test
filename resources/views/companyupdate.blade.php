@extends('maincompany')

@section('content')


    <form action="/company/edit/{{$company->id}}" method="POST">
        @csrf
          
        <input type="text" name="title" value="{{$company->title}}" placeholder="Название">
        <input type="text" name="content" value="{{$company->content}}" placeholder="Описание">
        <input type="text" name="adress" value="{{$company->adress}}" placeholder="Адрес">
        <input type="text" name="working" value="{{$company->working}}" placeholder="Время работы">
        <input type="text" name="phone" value="{{$company->phone}}" placeholder="Телефон">

        <button>Сохранить</button>
    </form>
    
    @endsection