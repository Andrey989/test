@extends('maincompany')

@section('content')

    <a href="/company">Главная</a>

    <div class="w3-container">
        <h2>{{$company->title}}</h2>
   
      
        <table class="w3-table">
          <tr>
            <th>Название</th>
            <th>Описание</th>
            <th>Адрес</th>
            <th>Время работы</th>
            <th>Телефон</th>
            <th>Удалить</th>
          
          </tr>

         
          <tr>
            <td>{{$company->title}}</td>
            <td>{{$company->content}}</td>
            <td>{{$company->adress}}</td>
            <td>{{$company->working}}</td>
            <td>{{$company->phone}}</td>
            <td><a href="/company/destroy/{{$company->id}}" class="w3-button w3-black">Удалить</a></td>

          </tr>
     

        </table>
      </div>



      <div class="w3-container">
        <h2>Работники: </h2>
   
      
        <table class="w3-table">
          <tr>
            <th>ФИО</th>
            <th>Контакты</th>
            <th>Адрес</th>
            <th>Должность</th>
            <th>Статус</th>
          </tr>

         
          @foreach ($wrkusers as $item)
          <tr>
            <td>{{$item->name}}</td>
            <td>{{$item->phone}}</td>
            <td>{{$item->adress}}</td>
            <td>{{$item->position}}</td>
            <td>{{$item->status}}</td>
            <td><a href="/wrkusers/edit/{{$item->id}}" class="w3-button w3-black">Редактировать</a></td>

          </tr>


        
        
        @endforeach

          
     

        </table>
      </div>



   
        
        <a href="/workuser/create/{{$company->id}}" class="w3-button w3-black">Добавить работника</a>
       

    
    @endsection