@extends('maincompany')

@section('content')

    <a href="/company/more/{{$id}}">Назад</a>
    <h2>Добавить работника: </h2>
    <form action="/workuser/create/{{$id}}" method="POST">
        @csrf
        <label for="">ФИО</label>
        <input type="text" name="name">
        <label for="">Должность</label>
        <input type="text" name="position">
        <label for="">Адрес</label>
        <input type="text" name="adress">
        <label for="">Телефон</label>
        <input type="text" name="phone">
        <label for="">Статус</label>
        <input type="text" name="status">

        <button>Сохранить</button>

    </form>
    
    @endsection