@extends('maincompany')

@section('content')

    

    <form action="/wrkusers/edit/{{$wrkuser->id}}" method="POST">
        @csrf

        <input type="text" name="name" value="{{$wrkuser->name}}" placeholder="ФИО">
        <input type="text" name="phone" value="{{$wrkuser->phone}}" placeholder="Телефон">
        <input type="text" name="adress" value="{{$wrkuser->adress}}" placeholder="Адрес">
        <input type="text" name="position" value="{{$wrkuser->position}}" placeholder="Должность">
        <input type="text" name="status" value="{{$wrkuser->status}}" placeholder="Статус">

        <button>Сохранить</button>


    </form>

    <a href="/company/more/{{$wrkuser->userid}}">Назад</a>

    @endsection