@extends('maincompany')

@section('content')


    @if (!empty($companys))




    <div class="w3-container">
        <h2>Филиалы: </h2>
   
      
        <table class="w3-table">
          <tr>
            <th>Название</th>
            <th>Описание</th>
            <th>Адрес</th>
            <th>Время работы</th>
            <th>Телефон</th>
            <th>Подробнее</th>
            <th>Редактировать</th>
          </tr>

          @foreach ($companys as $item)

          <tr>
            <td>{{$item->title}}</td>
            <td>{{$item->content}}</td>
            <td>{{$item->adress}}</td>
            <td>{{$item->working}}</td>
            <td>{{$item->phone}}</td>
            <td><a href="/company/more/{{$item->id}}" class="w3-button w3-black">Подробнее</a></td>
            <td><a href="/company/edit/{{$item->id}}" class="w3-button w3-black">Редактировать</a></td>
          </tr>
          
      @endforeach


        </table>
      </div>

    @endif


    <a href="/company/create" class="w3-button w3-black">Создать</a>

    @endsection