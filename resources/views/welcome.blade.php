@extends('main')

@section('content')
<main class="main-index">
  <section class="discount-cards">
    <div class="swiper-container discount-cards__swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-a.png') }}" alt="discount" class="discount-cards__img" />
        </div>
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-b.png') }}" alt="discount" class="discount-cards__img" />
        </div>
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-c.png') }}" alt="discount" class="discount-cards__img" />
        </div>
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-d.png') }}" alt="discount" class="discount-cards__img" />
        </div>
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-b.png') }}" alt="discount" class="discount-cards__img" />
        </div>
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-c.png') }}" alt="discount" class="discount-cards__img" />
        </div>
        <div class="swiper-slide">
          <img src="{{ asset('/images/discount-d.png') }}" alt="discount" class="discount-cards__img" />
        </div>
      </div>
    </div>
  </section>

  <section class="basket-desktop">
    <div class="moving-basket">
      <div class="basket__header">
        Корзина &nbsp;&nbsp;
        <span class="basket__header-count">(0)</span>
        <span class="basket__go-to-empty">&#xe802;</span>
        <a class="popup-link display-none" href="#popup-basket-empty">&#xe802;</a>
      </div>
      <div class="basket__content display-none">
        <div class="basket__restaurants"></div>
        <div class="basket__cost">
          <div class="basket__cost-delivery">
            <strong>Доставка</strong>
            <span> <span class="basket__delivery-summ">0</span> &#8381;</span>
          </div>
          <div class="basket__cost-total">
            <strong>Итого</strong>
            <span>
              <strike class="basket__cost-total-old"><span class="basket__cost-summ-old"></span>&#8381;</strike>
              <span> <span class="basket__total-summ"></span> &#8381; </span>
            </span>
          </div>
        </div>
        <div class="basket__inputs">
          <div class="basket__promo-code">
            <input type="text" placeholder="Введите промокод" class="input-default" />
          </div>
          <a href="#popup-order" class="btn-default popup-link basket__send-order">
            Сделать заказ
            <span class="basket__total-summ-btn"></span>&nbsp;&#8381;
          </a>
        </div>
      </div>
      <div class="basket__totally-empty">Корзина пуста. Выберите блюдо из меню или повторите предыдущий заказ</div>
    </div>
    <div class="basket__item basket__clone-item display-none">
      <div class="basket__your-order">
        <div class="basket__your-order-text">
          <div>Ваш заказ в рестране</div>
          <div class="basket__restaurant-name"></div>
        </div>
        <img src="" alt="restaurant-logo" />
      </div>
    </div>
    <!-- класс removable нужен для popup.js, т.к. если мы удаляем последнее блюдо в соотв. ресторане, удаляется сначала его 
          родительский элемент (basket__item), после чего теряется цепь поиска closed(".className") -->
    <div class="basket__ordered-dish basket__clone-ordered-dish display-none removable">
      <div class="basket__ordered-dish-text">
        <div class="basket__ordered-dish-name">
          <div class="basket__dish-title"></div>
          <div class="basket__ordered-dish-extra"></div>
        </div>
        <div class="basket__ordered-dish-img">
          <img alt="dish-img" />
          <span class="basket__ordered-dish-remove"> &#10005; </span>
          <span class="basket__ordered-dish-summ"></span>
        </div>
      </div>
      <div class="basket__ordered-dish-count">
        <div class="basket__count">
          <span class="btn-basket-remove">&ndash;</span>&nbsp;
          <span class="btn-basket-add">+</span>
        </div>
        <div class="basket__ordered-dish-price"><span class="basket__dish-price-number"></span> &#8381;</div>
      </div>
      <hr class="basket__hr" />
    </div>
    <button class="basket__header-delete display-none"></button>
  </section>

  <section class="depart">
    <!-- <div class="depart__type">
      <div class="depart__type-active">Рестораны</div>
      <div>Магазины</div>
    </div> -->
    <div class="depart__form">
      <a href="#depart__find-dish" class="depart__link-anchor display-none"></a>
      <span id="depart__hidden-anchor"></span>
      <div class="depart__inp-dish-container hint-container">
        <input type="text" class="input-default depart__inp-dish hint-inp"
          placeholder="&#xe800;&nbsp; Ресторан или блюдо..." />
        <div class="hint-menu">
          @foreach ($restaurants as $item)
          <div class="depart__menu-item hint-item"><?= $item->title ?></div>
          @endforeach
        </div>
      </div>
      <div class="depart__dropdown dropdown">
        <button class="dropdown__btn-open">
          <!-- <img src="./assets/images/index/filter.png" alt="filter" class="depart__img-filter" /> -->
          <span class="depart__filter-dropdown-icon">&#xf1de;</span>
          <span class="depart__btn-filter-text">Фильтры</span>
        </button>
        <div class="dropdown__content">
          <div class="depart__dropdown-header">Сортировать:</div>
          <div class="radio-btn-container" data-sort="rate">
            <input type="radio" name="depart-radio" id="depart__choise-container-first-option" checked />
            <label for="depart__choise-container-first-option">
              <span class="radio-btn-container__dot-container">
                <span class="radio-btn-container__dot"></span>
              </span>
              по рейтингу заведения
            </label>
          </div>
          <div class="radio-btn-container" data-sort="status-low-high">
            <input type="radio" name="depart-radio" id="depart__choise-container-third-option" />
            <label for="depart__choise-container-third-option">
              <span class="radio-btn-container__dot-container">
                <span class="radio-btn-container__dot"></span>
              </span>
              по ценам, от низких к высоким
            </label>
          </div>
          <div class="radio-btn-container" data-sort="status-high-low">
            <input type="radio" name="depart-radio" id="depart__choise-container-second-option" />
            <label for="depart__choise-container-second-option">
              <span class="radio-btn-container__dot-container">
                <span class="radio-btn-container__dot"></span>
              </span>
              по ценам, от высоких к низким
            </label>
          </div>
          <div class="radio-btn-container" data-sort="time">
            <input type="radio" name="depart-radio" id="depart__choise-container-fourth-option" />
            <label for="depart__choise-container-fourth-option">
              <span class="radio-btn-container__dot-container">
                <span class="radio-btn-container__dot"></span>
              </span>
              по скорости доставки
            </label>
          </div>
          <button class="dropdown__btn-close">Применить</button>
        </div>
      </div>
    </div>
  </section>

  <section class="dishes-search">
    <div class="dishes-search__relative-container">
      <div class="swiper-container dishes-search__swiper-container">
        <div class="swiper-wrapper">
          @foreach($disfilters as $item)
          <div class="swiper-slide dishes-search__img-wrapper">
            <img src="<?= $item->icon ?>" alt="" class="dishes-search__img" />
            <span class="dishes-search__name"><?= $item->name ?></span>
            @if (Auth::check() && Auth::user()->status > 0)
            <a class="edit-admin" href="/editdishfilter/<?= $item->id ?>">&#xe801;</a>
            @endif
          </div>
          @endforeach
          @if (Auth::check() && Auth::user()->status > 0)
          <a href="/dishesfilter" class="edit-admin edit-admin_static swiper-slide"> <span>&#xe801;</span>
            <span>Добавить</span></a>
          @endif
        </div>
      </div>
      <div class="dishes-search__btn-prev swiper__btn">&#xf007;</div>
      <div class="dishes-search__btn-next swiper__btn">&#xf006;</div>
    </div>
  </section>

  <section class="restaurants">
    <div class="restaurants__content">
      @foreach ($restaurants as $item)
      <div
        class="restaurants__restaurant aos aos_animate @if(date('H:i') > $item->timeworkopen && date('H:i') < $item->timeworkclose) open @else closed @endif"
        data-cooking-time="<?= $item->time ?>" data-restaurant-id="<?= $item->id ?>">
        <div class="restaurants__restaurant-img-wrapper">
          <img class="restaurants__img-main" src="<?= $item->picture ?>" alt="restaurant-img" />
          @if (Auth::check() && Auth::user()->status > 0)
          <a href="{{route('restaurantedit', $item->id)}}" class="edit-admin">&#xe801;</a>
          @endif
          <span class="restaurants__img-wrapper-extra-info-a">

            @if($item->saleflag == 1)
            <span class="restaurants__img-wrapper-high-rate" style="background-color: #da291c;">Акции</span>
            @endif
            @if($item->ratingflag == 1)
            <span class="restaurants__img-wrapper-high-rate">Высокий рейтинг</span>
            @endif
            @if($item->popularflag == 1)
            <span class="restaurants__img-wrapper-popular-place">Популярное место</span>
            @endif
            <!-- <span class="restaurants__img-wrapper-high-rate">Высокий рейтинг</span>
            <span class="restaurants__img-wrapper-popular-place">Популярное место</span> -->
          </span>
          <span class="restaurants__img-wrapper-extra-info-b">
            <img src="<?= $item->logo ?>" alt="logo" />
          </span>
          <div class="restaurants__img-wrapper-extra-info-c">
            Доставка не раньше <br />
            времени открытия
          </div>
        </div>
        <div class="restaurants__swiper-container">
          <div class="swiper-container restaurants__swiper">
            <div class="swiper-wrapper"></div>
          </div>
          <div class="restaurants__btn-prev swiper__btn">&#xf007;</div>
          <div class="restaurants__btn-next swiper__btn">&#xf006;</div>
          <div class="swiper-slide restaurants__swiper-slide-clone">
            <div class="restaurants__swiper-slide-img">
              <img class="restaurants__dish-img" alt="dish" />
              <span class="restaurants__img-wrapper-extra-info-b">
                <img src="<?= $item->logo ?>" alt="logo" />
              </span>
            </div>
            <div class="restaurants__dish-info">
              <span><span class="restaurants__dish-cost"></span>&#8381;</span>
              <span class="restaurants__dish-weight"></span>
            </div>
            <div class="restaurants__dish-name"></div>
          </div>
        </div>
        <h2 class="restaurants__name"><?= $item->title ?></h2>
        <div class="restaurants__details">
          <div class="restaurants__details-a">
            <div class="restaurants__a-container-rate">
              <span>&#xe808;&nbsp;</span>
              <span class="restaurants__restaurant-rate"><?= $item->rating ?></span>
            </div>
            <div class="restaurants__a-container-kitchen-type">
              <span class="hat-chef">
                <span></span>
                <span></span>
                <span>
                  <hr />
                </span>
              </span>
              <?= $item->kitchen ?>
            </div>
            <div class="restaurants__a-container-status">
              @if($item->status == 0)
              <div class="circle circle_solid">&#8381;</div>
              &nbsp;
              <div class="circle ">&#8381;</div>
              &nbsp;
              <div class="circle">&#8381;</div>
              @elseif($item->status == 1)
              <div class="circle circle_solid">&#8381;</div>
              &nbsp;
              <div class="circle circle_solid">&#8381;</div>
              &nbsp;
              <div class="circle">&#8381;</div>
              @elseif($item->status == 2)
              <div class="circle circle_solid">&#8381;</div>
              &nbsp;
              <div class="circle circle_solid">&#8381;</div>
              &nbsp;
              <div class="circle circle_solid">&#8381;</div>
              @endif
            </div>
          </div>
          <div class="restaurants__details-b">
            <div class="icon-circle">
              &#xe80a;
            </div>
            <div>
              <?php date_default_timezone_set('UTC'); ?>
              Открыто с <?= $item->timeworkopen ?> до <?= $item->timeworkclose ?>
            </div>
          </div>
          <div class="restaurants__details-c">
            <div class="restaurants__c-deilvery-cost">
              <div class="with-car">
                <img src="{{ asset('/images/car.svg') }}" alt="car" />
              </div>
              <?= $item->deliveryprice ?> &#8381;, бесплатно от <?= $item->deliveryfree  ?> &#8381;
            </div>
            <div class="restaurants__c-mins"><span class="delivery-time"
                data-cooking="<?= $item->time ?>"><?= $item->time + 10 ?></span> мин</div>
          </div>
        </div>
        <?php $dishes =  $item->dishes ?>
        <?php $arr = array(); ?>
        @foreach ($dishes as $key => $value)
        <?php $arr[$key] = $value->name ?>
        @endforeach
        <input class="status__inp-hidden" type="hidden" value="<?= $item->status ?>" />
        <input class="category__inp-hidden" type="hidden" value="@foreach($resfls as $filters) @if($item->id ==
        $filters->id_rest) <?= '[' . $filters->rubric . ']' ?> @endif @endforeach" />
        <input class="dishes__inp-hidden" type="hidden"
          value="@foreach ($arr as $value)<?= '[' . $value . ']' ?> @endforeach" />
        <input class="adress" type="hidden" value="<?= $item->adress ?>">
        <a href="/restdishes/<?= $item->id ?>" class="display-none restaurants__restaurant-link"></a>
      </div>
      @endforeach
    </div>
    <div class="restaurants__loading">
      <img src="{{ asset('/images/loading.svg') }}" alt="loading" />
    </div>
  </section>
</main>
@endsection