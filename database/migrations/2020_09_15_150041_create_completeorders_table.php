<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompleteordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('completeorders', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->string('portion');//Тип integer проверить какие данные передаются при save
            $table->string('name');
            $table->string('restaurant_name');
            $table->integer('priceportion')->nullable();
            $table->integer('price');
            $table->integer('quantity');
            $table->integer('restaurant_id');
            $table->string('category');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('completeorders');
    }
}
