<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Dishes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('restaurant_id');

            $table->text('picture')->nullable();
            $table->text('name');
            $table->integer('price');
            $table->text('content');
            $table->text('weight')->nullable();
            $table->text('custom')->nullable();
            $table->text('category');
            $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Dishes');
    }
}
