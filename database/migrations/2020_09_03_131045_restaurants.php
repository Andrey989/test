<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Restaurants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Restaurants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('logo')->nullable();
            $table->text('picture')->nullable();
            $table->text('title');
            $table->integer('deliveryprice');
            $table->integer('deliveryfree');
            $table->text('kitchen');
            $table->integer('time');
            $table->float('rating');
            $table->text('status');
            $table->text('content');
            $table->text('spec')->nullable();
            $table->text('timeworkopen')->nullable();
            $table->text('timeworkclose')->nullable();
            $table->text('payment')->nullable();
            $table->text('adress')->nullable();
            $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Restaurants');
    }
}
