<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoryusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storyusers', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->integer('user_id');
            $table->integer('dish_id');
            $table->string('picture_dish');
            $table->string('name');
            $table->integer('price');
            $table->integer('count');
            $table->string('portion');
            $table->string('category');
            $table->string('restaurant_name');
            $table->integer('restaurant_id');
            $table->integer('priceportion')->nullable();
            $table->integer('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storyusers');
    }
}
